import React ,{ useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Label, FormGroup, Input } from "reactstrap";
import {loadUsers} from "../../containers/Users/actions";

export const ContactPersonSelector = ({ value, onChange }) => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(loadUsers());
  }, []);

  const { users, selected, error, hash } = useSelector((state) =>
    state.usersList.toJS()
  );

  return (
    <FormGroup>
      <Label>Contact Person</Label>
      <Input type="select" value={value} onChange={onChange}>
        {users.filter(user => user.user_type === 3).map((user, index) => {
          return <option key={index} value={user.id}>{user.user_name}</option>;
        })}
      </Input>
    </FormGroup>
  );
};
